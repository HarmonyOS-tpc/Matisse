package com.zhihu.matisse.sample;

import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MatisseAbility;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;
import com.zhihu.matisse.engine.impl.PicassoEngine;
import com.zhihu.matisse.filter.Filter;
import com.zhihu.matisse.sample.slice.MainAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.window.service.Window;
import ohos.bundle.IBundleManager;
import ohos.utils.net.Uri;

import java.util.ArrayList;

public class MainAbility extends Ability {
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 123123;
    private static final int REQUEST_CODE = 23;
//    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 23;

    private MainProvider provider;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        Window window = getWindow();

        window.setTransparent(true);

        Button button = (Button) findComponentById(ResourceTable.Id_btnMain_zhihu);
        Button gif = (Button) findComponentById(ResourceTable.Id_btnMain_gif);
        ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_listMain);

        provider = new MainProvider(getContext(),new ArrayList<>(),new ArrayList<>());
        listContainer.setItemProvider(provider);

        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (verifySelfPermission("ohos.permission.WRITE_USER_STORAGE") != IBundleManager.PERMISSION_GRANTED ||
                        verifySelfPermission("ohos.permission.CAMERA") != IBundleManager.PERMISSION_GRANTED ||
                        verifySelfPermission("ohos.permission.READ_MEDIA") != IBundleManager.PERMISSION_GRANTED) {
                    if (canRequestPermission("ohos.permission.READ_MEDIA")) {
                        requestPermissionsFromUser(
                                new String[]{"ohos.permission.READ_MEDIA",
                                        "ohos.permission.WRITE_MEDIA",
                                        "ohos.permission.MEDIA_LOCATION",
                                        "ohos.permission.CAMERA",
                                        "ohos.permission.WRITE_USER_STORAGE"
                                }, MY_PERMISSIONS_REQUEST_CAMERA);
                    } else {

                    }
                } else {
                    Matisse.from(MainAbility.this)
                            .choose(MimeType.ofAll())
                            .addFilter(new GifSizeFilter(320, 320, 5 * Filter.K * Filter.K))
                            .countable(true)
                            .capture(true)
                            .maxSelectable(5)
                            .imageEngine(new GlideEngine())
                            .originalEnable(false)
                            .forResult( REQUEST_CODE);
                }

            }
        });

        gif.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (verifySelfPermission("ohos.permission.WRITE_USER_STORAGE") != IBundleManager.PERMISSION_GRANTED ||
                        verifySelfPermission("ohos.permission.CAMERA") != IBundleManager.PERMISSION_GRANTED ||
                        verifySelfPermission("ohos.permission.READ_MEDIA") != IBundleManager.PERMISSION_GRANTED) {
                    if (canRequestPermission("ohos.permission.READ_MEDIA")) {
                        requestPermissionsFromUser(
                                new String[]{"ohos.permission.READ_MEDIA",
                                        "ohos.permission.WRITE_MEDIA",
                                        "ohos.permission.MEDIA_LOCATION",
                                        "ohos.permission.CAMERA",
                                        "ohos.permission.WRITE_USER_STORAGE"
                                }, MY_PERMISSIONS_REQUEST_CAMERA);
                    } else {

                    }
                } else {
                    Matisse.from(MainAbility.this)
                            .choose(MimeType.of(MimeType.GIF),false)
                            .showSingleMediaType(true)
                            .countable(true)
                            .capture(true)
                            .maxSelectable(5)
                            .originalEnable(false)
                            .imageEngine(new GlideEngine())
                            .forResult(REQUEST_CODE);
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsFromUserResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                // 匹配requestPermissions的requestCode
                if (grantResults.length > 0
                        && grantResults[0] == IBundleManager.PERMISSION_GRANTED) {
                    // 权限被授予
                    // 注意：因时间差导致接口权限检查时有无权限，所以对那些因无权限而抛异常的接口进行异常捕获处理
                    Matisse.from(MainAbility.this)
                            .choose(MimeType.ofAll())
                            .addFilter(new GifSizeFilter(320, 320, 5 * Filter.K * Filter.K))
                            .countable(true)
                            .capture(true)
                            .maxSelectable(5)
                            .imageEngine(new GlideEngine())
                            .originalEnable(false)
                            .forResult( REQUEST_CODE);

                } else {
                    // 权限被拒绝
                }
                return;
            }
        }
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);

        if (resultCode != MatisseAbility.RESULT_OK){
            return;
        }
        try {
            ArrayList<Uri> uriArrayList = resultData.getSequenceableArrayListParam(MatisseAbility.EXTRA_RESULT_SELECTION);
            ArrayList<String> stringArrayList = resultData.getStringArrayListParam(MatisseAbility.EXTRA_RESULT_SELECTION_PATH);
            provider.setData(uriArrayList,stringArrayList);
        }catch (Exception e){
            throw new ClassCastException("Class Exception");
        }

    }

}
