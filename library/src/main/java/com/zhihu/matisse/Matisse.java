/*
 * Copyright 2017 Zhihu Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zhihu.matisse;

import com.zhihu.matisse.annotation.NonNull;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.utils.net.Uri;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Set;

/**
 * Entry for Matisse's media selection.
 */
public final class Matisse {

    private final WeakReference<Ability> mContext;
    private final WeakReference<Fraction> mFraction;
    private final WeakReference<AbilitySlice> mAbilitySlice;

    private Matisse(Ability ability) {
        this(ability, null,null);
    }

    private Matisse(Fraction fraction) {
        this(fraction.getFractionAbility(), fraction,null);
    }

    private Matisse(AbilitySlice slice){
        this(slice.getAbility(),null,slice);
    }

    private Matisse(Ability activity, Fraction fraction,AbilitySlice slice) {
        mContext = new WeakReference<>(activity);
        mFraction = new WeakReference<>(fraction);
        mAbilitySlice = new WeakReference<>(slice);
    }

    /**
     * Start Matisse from an Ability.
     * <p>
     * This Activity's  will be called when user
     * finishes selecting.
     *
     * @param ability Ability instance.
     * @return Matisse instance.
     */
    public static Matisse from(Ability ability) {
        return new Matisse(ability);
    }

    /**
     * Start Matisse from a Fraction.
     * <p>
     * This Fraction's will be called when user
     * finishes selecting.
     *
     * @param fraction Fraction instance.
     * @return Matisse instance.
     */
    public static Matisse from(Fraction fraction) {
        return new Matisse(fraction);
    }

    /**
     * Start Matisse from a AbilitySlice.
     * <p>
     * This AbilitySlice's will be called when user
     * finishes selecting.
     *
     * @param slice AbilitySlice instance.
     * @return Matisse instance.
     */
    public static Matisse from(AbilitySlice slice) {
        return new Matisse(slice);
    }

    /**
     * Obtain user selected media' {@link Uri} list in the starting Ability or Fraction.
     * @param data intent
     * @return User selected media' {@link Uri} list.
     */
    public static List<Uri> obtainResult(Intent data) {
        return data.getSequenceableArrayListParam(MatisseAbility.EXTRA_RESULT_SELECTION);
    }

    /**
     * Obtain user selected media path list in the starting Ability or AbilitySlice.
     *@param data intent
     * @return User selected media path list.
     */
    public static List<String> obtainPathResult(Intent data) {
        return data.getStringArrayListParam(MatisseAbility.EXTRA_RESULT_SELECTION_PATH);
    }

    /**
     * Obtain state whether user decide to use selected media in original
     *
     * @param data intent
     * @return Whether use original photo
     */
    public static boolean obtainOriginalState(Intent data) {
        return data.getBooleanParam(MatisseAbility.EXTRA_RESULT_ORIGINAL_ENABLE, false);
    }

    /**
     * MIME types the selection constrains on.
     * <p>
     * Types not included in the set will still be shown in the grid but can't be chosen.
     *
     * @param mimeTypes types
     * @return {@link SelectionCreator} to build select specifications.
     * @see MimeType
     * @see SelectionCreator
     */
    public SelectionCreator choose(@NonNull Set<MimeType> mimeTypes) {
        return this.choose(mimeTypes,true);
    }

    /**
     * MIME types the selection constrains on.
     * <p>
     * Types not included in the set will still be shown in the grid but can't be chosen.
     *
     * @param mediaTypeExclusive Whether can choose images and videos at the same time during one single choosing
     *                           process. true corresponds to not being able to choose images and videos at the same
     *                           time, and false corresponds to being able to do this.
     * @param mimeTypes types
     * @return {@link SelectionCreator} to build select specifications.
     * @see MimeType
     * @see SelectionCreator
     */
    public SelectionCreator choose(@NonNull Set<MimeType> mimeTypes,boolean mediaTypeExclusive) {
        return new SelectionCreator(this, mimeTypes, mediaTypeExclusive);
    }

    Ability getAbility() {
        return mContext.get();
    }

    Fraction getFraction() {
        return mFraction != null ? mFraction.get() : null;
    }

    AbilitySlice getAbilitySlice() {
        return mAbilitySlice != null ? mAbilitySlice.get() : null;
    }
}
