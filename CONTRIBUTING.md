# Matisse is an Open Source Project

## You Should Know

- To contribute with a small fix, simply create a pull request.
- Better to open an issue to discuss with the team and the community if you're intended to work on something BIG. 
- Check out our [roadmap] to see if some features you want is on the way.
- Better to use English to open issues and pull requests.

## Code Style

- Right margin is 120 characters instead of the default 100 value.

And also run `./gradlew checkstyle` to check if there is any style issues before sending a PR.